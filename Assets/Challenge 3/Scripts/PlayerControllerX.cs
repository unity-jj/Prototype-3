﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerX : MonoBehaviour
{
  public bool gameOver;

  public float floatForce;
  private float gravityModifier = 1.5f;
  private float topLimit = 13f;
  private float boingImpulseTop = -5f;
  private float boingImpulseBottom = 10f;
  private Rigidbody playerRb;

  public ParticleSystem explosionParticle;
  public ParticleSystem fireworksParticle;

  private AudioSource playerAudio;
  public AudioClip moneySound;
  public AudioClip explodeSound;
  public AudioClip groundSound;


  // Start is called before the first frame update
  void Start()
  {
    Physics.gravity *= gravityModifier;
    playerAudio = GetComponent<AudioSource>();
    playerRb = GetComponent<Rigidbody>();

    // Apply a small upward force at the start of the game
    playerRb.AddForce(Vector3.up * 5, ForceMode.Impulse);

  }

  // Update is called once per frame
  void Update()
  {
    MaintainInBounds();

    if (gameOver) return;

    // While space is pressed and player is low enough, float up
    if (Input.GetKey(KeyCode.Space))
    {
      MoveBalloonUp();
    }
  }

  private void OnCollisionEnter(Collision other)
  {
    // if player collides with bomb, explode and set gameOver to true
    if (other.gameObject.CompareTag("Bomb")) CollidedWithBomb(other);
    // if player collides with money, fireworks
    else if (other.gameObject.CompareTag("Money")) CollidedWithMoney(other);
    else if (other.gameObject.CompareTag("Ground")) CollidedWithGround();
  }

  void MaintainInBounds()
  {
    if (transform.position.y <= topLimit) return;

    transform.position = new Vector3(transform.position.x, topLimit, transform.position.z);
    playerRb.velocity = Vector3.zero;
    playerRb.angularVelocity = Vector3.zero;
    ImpulseBalloon(boingImpulseTop);
  }

  void MoveBalloonUp()
  {
    playerRb.AddForce(Vector3.up * floatForce);
  }

  void ImpulseBalloon(float extraForce = 1)
  {
    playerRb.AddForce(Vector3.up * extraForce, ForceMode.Impulse);
  }

  void CollidedWithBomb(Collision other)
  {
    explosionParticle.Play();
    playerAudio.PlayOneShot(explodeSound, 1.0f);
    gameOver = true;
    Debug.Log("Game Over!");
    Destroy(other.gameObject);
  }

  void CollidedWithMoney(Collision other)
  {
    fireworksParticle.Play();
    playerAudio.PlayOneShot(moneySound, 1.0f);
    Destroy(other.gameObject);
  }

  void CollidedWithGround()
  {
    ImpulseBalloon(boingImpulseBottom);
    playerAudio.PlayOneShot(groundSound);
  }
}
