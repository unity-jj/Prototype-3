using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GlobalVariables
{
  public static bool isGameOver = false;
  public static bool hasStartedRunning = false;
}
