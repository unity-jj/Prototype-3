using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimator : MonoBehaviour
{
  private Animator playerAnimator;

  // Start is called before the first frame update
  void Start()
  {
    playerAnimator = GetComponent<Animator>();
  }

  public void SetRunning()
  {
    playerAnimator.SetFloat("Speed_f", 1);
  }

  public void SetDeath()
  {
    playerAnimator.SetBool("Death_b", true);
    playerAnimator.SetInteger("DeathType_int", 1);
  }

  public void SetJump()
  {
    playerAnimator.SetTrigger("Jump_trig");
  }
}
