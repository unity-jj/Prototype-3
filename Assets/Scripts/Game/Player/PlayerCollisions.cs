using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollisions : MonoBehaviour
{
  public AudioClip crashSound;

  private PlayerController playerController;
  private PlayerAnimator playerAnimator;
  private PlayerParticles playerParticles;

  // Start is called before the first frame update
  void Start()
  {
    playerController = GetComponent<PlayerController>();
    playerAnimator = GetComponent<PlayerAnimator>();
    playerParticles = GetComponent<PlayerParticles>();
  }

  // Update is called once per frame
  void Update()
  {

  }

  private void OnCollisionEnter(Collision collision)
  {
    if (collision.gameObject.CompareTag("Obstacle"))
    {
      GlobalVariables.isGameOver = true;
      playerParticles.explosionParticle.Play();
      playerParticles.dirtParticle.Stop();

      playerAnimator.SetDeath();
      GetComponent<AudioSource>().PlayOneShot(crashSound);
    }
    else if (collision.gameObject.CompareTag("Ground"))
    {
      GetComponent<PlayerJump>().ResetJumps();

      if (GlobalVariables.isGameOver || !GlobalVariables.hasStartedRunning) return;

      playerParticles.dirtParticle.Play();
    }
  }

  private void OnCollisionExit(Collision collision)
  {
    if (collision.gameObject.CompareTag("Ground"))
    {
      playerParticles.dirtParticle.Stop();
    }
  }
}
