using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
  private float gravityModifier = 2f;
  private float entranceSpeed = 2f;

  private PlayerAnimator playerAnimator;
  private PlayerParticles playerParticles;


  // Start is called before the first frame update
  void Start()
  {
    Physics.gravity *= gravityModifier;
    playerAnimator = GetComponent<PlayerAnimator>();
    playerParticles = GetComponent<PlayerParticles>();
    toggleScripts(false);
  }

  // Update is called once per frame
  void Update()
  {
    if (!GlobalVariables.hasStartedRunning)
    {
      MakeEntrance();
    }
  }

  void MakeEntrance()
  {
    if (transform.position.x >= -2.5)
    {
      StartRunning();
      return;
    }

    transform.Translate(Vector3.forward * entranceSpeed * Time.deltaTime);
  }

  void StartRunning()
  {
    GlobalVariables.hasStartedRunning = true;
    playerAnimator.SetRunning();
    playerParticles.dirtParticle.Play();
    toggleScripts(true);
  }

  void toggleScripts(bool active = true)
  {
    GetComponent<PlayerJump>().enabled = active;
    GetComponent<PlayerCollisions>().enabled = active;
  }
}
