using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDash : MonoBehaviour
{
  // Start is called before the first frame update
  void Start()
  {

  }

  // Update is called once per frame
  void Update()
  {
    if (InputIsDash())
    {
      Dash();
    }
    else
    {
      StopDash();
    }
  }

  bool InputIsDash()
  {
    return Input.GetKey(KeyCode.Z) ||
      Input.GetKey(KeyCode.E) ||
      Input.GetKey(KeyCode.RightArrow) ||
      Input.GetMouseButton(1);
  }

  void Dash()
  {
    Time.timeScale = 2;
  }

  void StopDash()
  {
    Time.timeScale = 1;
  }
}
