using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerJump : MonoBehaviour
{
  private float jumpForce = 700f;
  private int maxJumps = 2;
  private int currentJumps = 0;

  public AudioClip jumpSound;

  private PlayerAnimator playerAnimator;

  void Start()
  {
    playerAnimator = GetComponent<PlayerAnimator>();
  }

  void Update()
  {
    if (GlobalVariables.isGameOver)
    {
      this.enabled = false;
    }


    if (InputIsJump() && currentJumps < maxJumps)
    {
      Jump();
    }
  }

  bool InputIsJump()
  {
    return Input.GetKeyDown(KeyCode.Space) ||
      Input.GetKeyDown(KeyCode.W) ||
      Input.GetKeyDown(KeyCode.UpArrow) ||
      Input.GetMouseButtonDown(0);
  }

  void Jump()
  {
    currentJumps += 1;
    GetComponent<Rigidbody>().AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
    playerAnimator.SetJump();
    GetComponent<AudioSource>().PlayOneShot(jumpSound);
  }

  public void ResetJumps()
  {
    currentJumps = 0;
  }
}
