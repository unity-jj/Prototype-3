using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
  private int score = 0;

  // Start is called before the first frame update
  void Start()
  {
    InvokeRepeating("AddScore", 0, 1);
  }

  // Update is called once per frame
  void Update()
  {
    if (GlobalVariables.isGameOver)
    {
      gameObject.SetActive(false);
      CancelInvoke();
    }
  }

  public void AddScore()
  {
    score += 1 * (int)Time.timeScale;
    Debug.Log("Score: " + score);
  }
}
